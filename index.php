<?php
// Autoloading class
require_once './classes/Autoload.php';

// Set autoloader
$loader = new Tony\Autoload;
$loader->add('Tony\\', __DIR__ . '/classes/');
$loader->add('', __DIR__ . '/libs/templatePower/');
$loader->register();

// Start template engine
$tpl = new TemplatePower('./views/index.tpl');
$tpl->prepare();



$q1 = new Tony\Question1\FizzBuzz;
$tpl->assign('question1', implode(',&nbsp;', $q1->getFizzBuzz(100)));

$q2 = new Tony\Question2\Refactor;
$tpl->assign('question2', $q2->getRefactoredCode());

$q3 = new Tony\Question3\Refactor;
$tpl->assign('question3', $q3->getRefactoredCode());


$tpl->printToScreen();
?>