<?php
namespace Tony\Question1;

class FizzBuzz
{
    public function getFizzBuzz($maxNumber)
    {
        $ret = array();
        for ($i = 1; $i <= $maxNumber; $i++) {
            if ($i % 3 === 0) {
                $ret[$i] = 'Fizz';
                if ($i % 5 === 0) {
                    $ret[$i] .= 'Buzz';
                }
            } elseif ($i % 5 === 0) {
                $ret[$i] = 'Buzz';
            } else {
                $ret[$i] = (string)$i;
            }
        } // End for
        
        return $ret;
    }
}
?>