<?php
namespace Tony\Question2;

class Refactor
{
    public function getRefactoredCode()
    {
        // It's not a good thing to do...
        $code = '<?php
if ((isset($_SESSION[\'loggedin\']) && $_SESSION[\'loggedin\'] === true) '
    .'|| (isset($_COOKIE[\'Loggedin\']) && $_COOKIE[\'Loggedin\'] === true)) {
    header("Location: http://www.google.com");
    exit();
}';
        return str_replace(
            array('<', '>', "\n", ' '),
            array('&lt;', '&gt;', '<br>', '&nbsp;'),
            $code
        );
    }
}
?>