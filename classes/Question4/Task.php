<?php
namespace Tony\Question4;

class Task
{
    private $db;
    
    public $id;
    public $title;
    public $description;
    public $priority;
    
    public function __construct($db)
    {
        $this->db = $db;
    }
    
    public function load($id)
    {
        try {
            $stm = $this->db->prepare('SELECT * FROM task WHERE id = ?');
            $stm->execute(array($id));
            $task = $stm->fetchAll();
            if ($task) {
                $this->id = $task[0]['id'];
                $this->title = $task[0]['title'];
                $this->description = $task[0]['description'];
                $this->priority = $task[0]['priority'];
                return true;
            }
            return false;
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function save()
    {
        try {
            if (!$this->id) { // Insert
                $stm = $this->db->prepare(
                    'INSERT INTO task (title, description, priority) '
                    .'VALUES (?, ?, ?)'
                );
                $stm->execute(
                    array($this->title, $this->description, $this->priority)
                );
            } else { // Update
                $stm = $this->db->prepare(
                    'UPDATE task SET title = ?, description = ?, '
                    .'priority = ? WHERE id = ?'
                );
                $stm->execute(
                    array(
                        $this->title,
                        $this->description,
                        $this->priority,
                        $this->id
                    )
                );
            }
            return true;
        } catch (Exception $e) {
            return false;
        }    
    }
    
    public function delete()
    {
        $stm = $this->db->prepare('DELETE FROM task WHERE id = ?');
        $stm->execute(array($this->id));
    }
    
    public function getAll()
    {
        try {
            $stm = $this->db->prepare(
                'SELECT * FROM task ORDER BY priority DESC'
            );
            $stm->execute();
            return $stm->fetchAll();
        } catch (Exception $e) {
            return array();
        }
    }
}
?>