<?php
namespace Tony;

final class Aux {
    final public static function getIncomingVars() {
        $vars = array();
        parse_str(file_get_contents('php://input'), $vars);
        return $vars;
    }
}