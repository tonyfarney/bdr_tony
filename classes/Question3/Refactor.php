<?php
namespace Tony\Question3;

class Refactor
{
    public function getRefactoredCode()
    {
        // It's not a good thing to do... again
        $code = '<?php
class MyUserClass
{
    protected $dbconn;
    
    public function __construct($dbconn)
    {
        $this->dbconn = $dbconn;
    }
                
    public function getUserList()
    {
        return $this->dbconn->query(\'SELECT name FROM user ORDER BY name ASC\');
    }
}';
        return str_replace(
            array('<', '>', "\n", ' '),
            array('&lt;', '&gt;', '<br>', '&nbsp;'),
            $code
       );
    }
}
?>