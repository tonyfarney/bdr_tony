<?php
namespace Tony;

class Autoload {
    private $prefix = array();
    private $baseDir = array();
     
    private function autoload($request)
    {
        foreach ($this->prefix as $ind => $prefix) {
            $len = strlen($prefix);
            if (strncmp($prefix, $request, $len) !== 0) {
                continue; // Go to next prefix
            }
            $className = substr($request, $len); // Removes Prefix
            $file = $this->baseDir[$ind] . str_replace('\\', '/', $className) . '.php';
            if (file_exists($file)) {
                require_once $file;
                break;
            }
        }
    }
    
    public function add($prefix, $baseDir) {
        $this->prefix[] = $prefix;
        $this->baseDir[] = $baseDir;
    }
    
    public function register() {
        spl_autoload_register(array($this, 'autoload'));
    }
}
?>