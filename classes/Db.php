<?php
namespace Tony;

class Db {
    
    private static $db = null; // Singleton
     
    public static function getInstance()
    {
        if (self::$db) {
            return self::$db;
        }
        
        // Throws PDOException on error
        self::$db = new \PDO( // Hardcoded config... not a good idea
            'mysql:host=localhost;dbname=bdr', // Connection string
            'root', // Username
            'root', // Password
            array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8') // Options
        );
        self::$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        
        return self::$db;
    }
}
?>