$(window).load(function() {
    $('div.code').each(function(i, block) {
        hljs.highlightBlock(block); // Global highlight object
    });
    
    clearTaskFrm();
    
    $(document).on('click', '.edit-task', function() {
        editTask($(this).parents('tr:first').data('task'));
    })
    .on('click', '.delete-task', function() {
        if (confirm('Confirma exclusão?')) {
            deleteTask($(this).parents('tr:first').data('task'));
        }
    });
    
    
    getTasks();
    
    $('#btn-save-task').click(saveTask);
    $('#btn-new-task').click(function() {
        clearTaskFrm();
        $(this).hide();
        $('#title').focus();
    });
});

$(document).ajaxError(function(ret) {
    alert('Ocorreu um erro executando a requisição AJAX.');
});

function getTasks() {
    $.ajax({
        "type": 'GET',
        "url": 'tasks/',
        "dataType": "json",
        "success": function (ret) { // Render tasks
            if (ret.error) {
                alert(ret.msg);
                return;
            }
            $('#tasks tbody').empty(); // Clean tasks
            $.each(ret.tasks, function(ind, task) { // Render tasks
                $('#tasks tbody').append(
                    '<tr>'
                        +'<td>'+$('#priority option[value="'+task.priority+'"]').text()+'</td>'
                        +'<td>'+task.title+'</td>'
                        +'<td>'+task.description+'</td>'
                        +'<td class="text-center">'
                            +'<i class="glyphicon glyphicon-edit edit-task" title="Editar"></i>'
                            +'<i class="glyphicon glyphicon-trash delete-task" title="Excluir"></i>'
                        +'</td>'
                    +'</tr>'
                )
                .find('tr:last').data('task', task);
            });
        }
    });
}

function clearTaskFrm() {
    $('#btn-new-task').hide();
    $('#frm-task input[type="text"], input[type="hidden"]').val('');
    $('#frm-task select').each(function () {
        $(this).val($(this).find('option:first').val());
    });
}

function saveTask() {
    try {
        if ($.trim($('#title').val()) === '') {
            throw 'Título não informado.';
        }
        if ($.trim($('#description').val()) === '') {
            throw 'Descrição não informada.';
        }
    } catch (e) {
        alert(e);
        return;
    }
    
    var id = $.trim($('#id').val());
    $.ajax({
        "type": id === '' ? 'POST' : 'PUT', // Insert/update
        "url": 'tasks/'+id,
        "data": $('#frm-task').serialize(),
        "dataType": "json",
        "success": function (ret) { // Render tasks
            if (ret.error) {
                alert(ret.msg);
                return;
            }
            clearTaskFrm();
            getTasks(); // Refresh
        }
    });
}

function editTask(task) {
    clearTaskFrm();
    $('#btn-new-task').show();
    $('#id').val(task.id);
    $('#description').val(task.description);
    $('#priority').val(task.priority);
    $('#title').val(task.title).focus();
}

function deleteTask(task) {
    $.ajax({
        "type": 'DELETE',
        "url": 'tasks/'+task.id,
        "dataType": "json",
        "success": function (ret) { // Render tasks
            if (ret.error) {
                alert(ret.msg);
                return;
            }
            getTasks(); // Refresh
        }
    });
}