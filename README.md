# Prova Tony Farney

As respostas para as 4 questões da prova podem ser todas verificadas no navegador (mobile ou desktop), bastando acessar a página index do projeto.

### Instalação
Para executar o projeto, é necessário que você tenha um servidor Apache com PHP 5.3+ configurado, e com o módulo Rewrite habilitado. 

Para instalar o projeto basta clonar o repositório GIT em uma pasta servida pelo Apache. Ex:
```sh
$ git clone https://bitbucket.org/tonyfarney/bdr_tony.git
```


#### Configuração do Banco de Dados
Para tornar a questão 4 funcional, é necessário criar a base de dados em um SGBD MySQL. O script para criação da base e de sua estrutura encontra-se no arquivo:

  - /db.sql

Após a criação da base, é ncessário editar o arquivo:
  - /classes/Db.php

Nas linhas 16 a 18 deste arquivo devem ser alteradas as configurações de "host", "usuário" e "senha" de acordo com seu ambiente.
Obs: configuração hardcoded não é uma boa prática.

Ex:

```php
self::$db = new \PDO(
    'mysql:host=meu_dominio;dbname=bdr', // Alterar o host, se necessário
    'usuario_mysql', // Seu usuário MySQL
    'senha', // Sua senha MySQL
    array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8')
);
```

#### Execução

Para executar o projeto basta acessar pelo navegador o diretório onde você baixou o projeto. Ex: http://localhost/bdr/

Obs: Caso seu servidor não esteja configurado para tratar aquivos com nome "index.php" como arquivos de índice, basta acessar diretamente: http://localhost/bdr/index.php
