<?php
// Autoloading class
require_once './classes/Autoload.php';

// Set autoloader
$loader = new Tony\Autoload;
$loader->add('Tony\\', __DIR__ . '/classes/');
$loader->register();

$response = new stdClass();
$response->error = 0;
$response->msg = '';

try {
    $len = strlen(basename(__FILE__));
    $appDir = substr(
        $_SERVER['PHP_SELF'],
        0,
        strlen($_SERVER['PHP_SELF']) - $len
    );
    
    $url = $_SERVER['REQUEST_URI'];
    if (strpos($_SERVER['REQUEST_URI'], $appDir) === 0) {
        $url = substr($_SERVER['REQUEST_URI'], strlen($appDir));
    }
    
    $pos = strpos($url, '?');
    if ($pos !== false) {
        $url = substr($url, 0, $pos);
    }
    
    $path = explode('/', trim($url, '/'));
    $countPath = count($path);
    if ($countPath < 1 || $countPath > 2 || $path[0] != 'tasks') {
        throw new Exception('Invalid request.');
    }
    $task = new Tony\Question4\Task(Tony\Db::getInstance());
    if ($countPath == 2) {
        if (!$task->load(intval($path[1]))) {
            throw new Exception('Task not found.');
        }
    }
    
    switch ($_SERVER['REQUEST_METHOD']) {
        case 'GET': // Find
            $response->tasks = array();
            if ($countPath === 2) { // Getting task info
                $response->tasks[] = $task; 
            } else { // Getting all tasks
                $response->tasks = $task->getAll();
            }
            break;
        case 'POST': // Insert
            if ($countPath != 1) {
                throw new Exception('Invalid POST request.');
            }
            $task->title = $_POST['title'];
            $task->description = $_POST['description'];
            $task->priority = $_POST['priority'];
            $task->save();
            break;
        case 'PUT': // Update
            if ($countPath != 2) {
                throw new Exception('Invalid PUT request.');
            }
            $vars = Tony\Aux::getIncomingVars();
            $task->title = $vars['title'];
            $task->description = $vars['description'];
            $task->priority = $vars['priority'];
            $task->save();
            break;
        case 'DELETE': // Delete
            if ($countPath != 2) {
                throw new Exception('Invalid DELETE request.');
            }
            $task->delete();
            break;
        default:
            break;
    }
    
    
} catch (Exception $e) {
    $response->error = 1;
    $response->msg = $e->getMessage();
}

echo json_encode($response);

/*
echo '<pre>';
var_dump($_REQUEST);
var_dump($_SERVER);
echo __DIR__.'<br>';
echo __FILE__.'<br>';
echo $url;
*/
?>