<html>
    <head>
        <meta charset="utf-8" />
        <title>Tony - Prova BDR</title>
        <script src="libs/jquery/jquery.min.js"></script>
        <script src="libs/bootstrap/js/bootstrap.min.js"></script>
        <script src="libs/highlight/highlight.min.js"></script>
        <script src="js/default.js"></script>
        
        <link rel="stylesheet" href="libs/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="libs/highlight/default.min.css">
        <link rel="stylesheet" href="css/default.css">
    </head>
    <body>
        <div class="answer">
            <div class="title">Questão 1:</div>
            <div style="overflow-x: auto;">{question1}</div>
        </div>
        <div class="answer">
            <div class="title">Questão 2:</div>
            <div class="code">{question2}</div>
        </div>
        <div class="answer">
            <div class="title">Questão 3:</div>
            <div class="code php">{question3}</div>
        </div>
        <div class="answer">
            <div class="title">Questão 4:</div>
            <div id="container-tasks">
                <form id="frm-task" action="#" method="post">
                    <div class="row">
                        <div class="col-sm-3 col-md-3">
                            <label for="text">Título</label>
                            <input type="text" id="title" name="title" class="form-control" maxlength="15">
                            <input type="hidden" id="id" name="id">
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <label for="description">Descrição</label>
                            <input type="text" id="description" name="description" class="form-control" maxlength="100">
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <label for="priority">Prioridade</label>
                            <select id="priority" name="priority" class="form-control">
                                <option value="1">Muito Baixa</option>
                                <option value="2">Baixa</option>
                                <option value="3">Média</option>
                                <option value="4">Alta</option>
                                <option value="5">Muito Alta</option>
                            </select>
                        </div>
                    </div>
                </form>
                
                <div class="row actions">
                    <div class="col-sm-12 col-md-12">
                        <button type="button" id="btn-new-task" class="btn btn-success" style="margin-right: 5px;"><i class="glyphicon glyphicon-plus"></i> Nova</button>
                        <button type="button" id="btn-save-task" class="btn btn-primary"><i class="glyphicon glyphicon-ok"></i> Salvar</button>
                    </div>
                </div>
                
                <table id="tasks" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Prioridade</th>
                            <th>Título</th>
                            <th>Descrição</th>
                            <th class="text-center">Ações</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </body>
</html>